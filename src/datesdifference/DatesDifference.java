
package datesdifference;

import java.time.LocalDate;
import java.time.Month;
import java.time.Period;



public class DatesDifference {

    public static void main(String[] args) {

       

    LocalDate sinceGraduation = LocalDate.of(1984, Month.JUNE, 4);
    LocalDate currentDate = LocalDate.now();

    Period betweenDates = Period.between(sinceGraduation, currentDate);

    int diffInDays = betweenDates.getDays();
    int diffInMonths = betweenDates.getMonths();
    int diffInYears = betweenDates.getYears();

    logger.info(diffInDays);
    logger.info(diffInMonths);
    logger.info(diffInYears);

    assertTrue(diffInDays >= anyInt());
    assertTrue(diffInMonths >= anyInt());
    assertTrue(diffInYears >= anyInt());

    }

    private static int anyInt() {
        return 0;
    }

    private static void assertTrue(boolean b) {
    }
    
}
